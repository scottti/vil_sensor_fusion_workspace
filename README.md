
__This README only contains documentation for setting up this workspace. For more documentation on the actual
VIL fusion project, see the README in the `vil_sensor_fusion` repository (Included as a submodule in 
`src/vil_sensor_fusion`).__

This repository contains my entire Catkin workspace, where each package is a submodule. I have set up this repo
because of the following unique circumstances:

 - I am experimenting with a custom fork of Rovio. It is included as a submodule here.
 - There is a known issue with LOAM and PCL, meaning PCL has to be built from source. That source is included here
   as a submodule.
 - I need to use a custom version of LOAM, so the LOAM submodule here points to a fork of LOAM.

Because of these various circumstances, getting everything working is non-trivial. Here are some directions:

- Clone all submodules:
    - `git submodule update --init --recursive`
- Install GTSAM globally (Do not rely on Catkin to build GTSAM!)
    - Follow instructions [here](https://gtsam.org/build/)
- Fix the issue with LOAM and PCL (Point Cloud Library)
    - Issue is described [here](https://github.com/laboshinl/loam_velodyne/issues/71)
    - Uninstall the `apt`-provided `libpcl`: `sudo apt remove --purge libpcl*`
    - Install from source:
        - `cd src/pcl`
        - `mkdir build`
        - `cd build`
        - `cmake -DCMAKE_RELEASE_TYPE=Release ..`
        - `make -j8`
        - `sudo make install`
    - Reinstall the ROS bindings for PCL:
        - `sudo apt install ros-melodic-pcl-ros ros-melodic-pcl-conversions`
- Build Carla from source
    - There is a bug in Carla, which I have fixed in [this PR](https://github.com/carla-simulator/carla/pull/2795).
     The following instructions assume that the pre-built Carla apt package has not yet been updated, so you
     must build from source. If the package has been updated, ignore all of these instructions and instead 
     install the apt package.
    - Follow the [official Carla instructions](https://carla.readthedocs.io/en/latest/build_linux/) for building
      from source.
    - In my case, I was unable to use the recommended `make launch` option, because Unreal Editor crashed constantly.
      I had to use `make package`. Your results may vary.
    - To run Carla, run the following script in the `carla` source code folder:
        - `./Dist/CARLA_Shipping_0.9.9-dirty/LinuxNoEditor/CarlaUE4.sh -opengl`
        - (This script will only be here if you used `make package` in the previous step)
    - When using Carla, it seems to leak memory. If you leave the server running
     for a long time, it will eventually segfault and crash. Before each simulation,
     you should stop and restart Carla.
- Once everything is built, refer to the README in `src/vil_sensor_fusion` for details on running simulations and
  experiments.